Anomaly had recently been awarded Ad-Age's Agency of the Year, so working with with creative team <a href="https://www.peterjostrand.com/" target="_blank">Pete Jostrand</a> and <a href="https://www.josh-chua.com/" target="_blank">Josh Chua</a>, we asked our interns the question, Are You Better Than Our Worst?

![](/img/portfolio/anomaly-areyoubetterthanourworst/areyoubetterthanourworst.png#center)

![](/img/portfolio/anomaly-areyoubetterthanourworst/areyoubetterthanourworst.gif#center)


I built the site on Hugo static site generator. It features a custom video navigator...

![](/img/portfolio/anomaly-areyoubetterthanourworst/video-nav.gif#center)

And a sticky CTA, to ensure the apply button was never too far away - over 6 weeks we got **over 2000 signups**.

I also pushed our narrative digitally to quickly turn the site into interactive platform that built hype amongst our applicants. My favorite being a blank Google Doc for our hopefulls to 'Do Leanne's Work' in. This took on a life of its own, served as communication between our applicants and even brought in rival agencies that were trying to advertise on our platform! Touché.

![](/img/portfolio/anomaly-areyoubetterthanourworst/googledocs.gif#center)