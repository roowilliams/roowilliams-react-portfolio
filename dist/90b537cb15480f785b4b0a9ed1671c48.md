+++
draft = false
image = "img/portfolio/tmw-pitchit/Gameshow-Timer-Creative_Showtime-01.jpg"
showonlyimage = false
date = "2014-07-16"
title = "Pitchit!"
weight = 11
blurb = "An Arduino-powered, self-contained gameshow host."
tech = [ 
	'Arduino',
	'3D Printing'
]

+++

<img src="/img/portfolio/tmw-pitchit/Gameshow-Timer-Creative_Showtime.jpg" />

### Context
Every other Friday the Creative department of TMW Unlimited get together to shout about any fun projects they've been working on. This takes the shape of a gameshow, with each of the 'contestants' given 30 seconds to pitch their project. Judges award the best projects with cupcakes and alcohol.

### Brief
After a few shonky shows run with soundboards and stopwatches I was briefed to design a timer that would run the show automatically. The timer was to count down 30 seconds, buzz and then give a 5 second pause to change contestants before counting down again. There could be a variable number of contestants each week so the timer would need a way for the user to select how many 'rounds' there needed to be.

### Result
<video width="100%" preload="auto" controls> 
	<source src="/img/portfolio/tmw-pitchit/Gameshow_Timer.mp4" type="video/mp4" />
	Your browser does not support the video tag.
</video>



### Design & Build
The interaction model was simple. The user would switch the device on, use the knob to select how many rounds they wanted, represented by how many lights were glowing, and then press the button to start the game.

Sound came from an Adafruit sound shield, and some small speakers and a potentiometer was used in combination with a button as the only sources of input. Once I had the electronics figured out I moved on to constructing an enclosure using a combination of PVC drainpipe and 3D printed parts.

<img src="/img/portfolio/tmw-pitchit/Gameshow-Timer-Shift_Register_Test_Arduino.jpg" />

<img src="/img/portfolio/tmw-pitchit/Gameshow-Timer-Shift_Register-Proto_Board_Soldering-Arduino.jpg" />

<img src="/img/portfolio/tmw-pitchit/Gameshow-Timer-CAD_Exploded_View.jpg" />

<img src="/img/portfolio/tmw-pitchit/Gameshow-Timer-3D_Printed_End_Cap_With_Speaker.jpg" />

<div class="gallery-grid">
<div class="row">

<div class="col-xs-6 col-sm-3">
<img src="/img/portfolio/tmw-pitchit/Gameshow-Timer-3D_Printing_End_Cap.gif" alt="" />
</div>
<div class="col-xs-6 col-sm-3">
	<img src="/img/portfolio/tmw-pitchit/Gameshow-Timer-3D_Printed_End_Cap_With_Speaker_Assembly.gif" alt="" />
</div>

<div class="col-xs-6 col-sm-3">
<img src="/img/portfolio/tmw-pitchit/Gameshow-Timer-3D_Printing_LED_Bezel_Clip.gif" alt="" />
</div>
<div class="col-xs-6 col-sm-3">
<img src="/img/portfolio/tmw-pitchit/Gameshow-Timer-3D_Printed_LED_Bezel_Assembly.gif" alt="" />
</div>

</div>
</div>

<img src="/img/portfolio/tmw-pitchit/Gameshow-Timer-Build_Chaos.jpg" />
<img src="/img/portfolio/tmw-pitchit/Gameshow-Timer-Testing.gif#center" title="" alt=""/>
