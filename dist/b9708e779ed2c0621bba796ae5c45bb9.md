+++
draft = false
image = "img/portfolio/meals-on-wheels-savelunch/featured-image.jpg"
showonlyimage = false
date = "2016-11-05T20:02:19+05:30"
title = "Meals on Wheels America: Savelunch"
weight = 6
blurb = "Helping Meals on Wheels America rally against proposed budget cuts."
tech = [ 
	'HTML5/CSS3',
	'Javascript',
	'Bulma'
]

[[collaborators]]
	name = 'Westley Taylor'
	url = 'http://www.wesandwan.com/'

[[collaborators]]
	name = 'Wan Kang'
	url = 'http://www.wesandwan.com/'

[[links]]
	name = "Savelunch.org"
	url = 'http://savelunch.org'

+++

When the Trump administration announced budget plans that would severely impact Meals on Wheels, we saw an opportunity to help them push back. Working with creative team Westley Taylor and Wan Kang at Anomaly we created Savelunch.org to seed a campaign that encouraged, and made it easy for people to activate against the proposed cuts by either tweeting their representatives, skipping a meal and posting a photo of their empty plate on social media, or by emailing congress directly.

![](/img/portfolio/meals-on-wheels-savelunch/screen-recording.gif#center)


![](/img/portfolio/meals-on-wheels-savelunch/instagram-post-1.jpg)

![](/img/portfolio/meals-on-wheels-savelunch/instagram-post-2.jpg)

![](/img/portfolio/meals-on-wheels-savelunch/instagram-post-3.jpg)

![](/img/portfolio/meals-on-wheels-savelunch/instagram-post-4.jpg)
