import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Header from './Header.jsx'
import Home from './Home.jsx'
import MarkDowner from './MarkDowner.jsx'
import Workshop from './Workshop.jsx'
import ArticleContainer from './ArticleContainer.jsx'
import '../styles/App.scss' // global styles

class App extends React.Component {
	render () {
		return (
			<div>
				<Header />			
				<Switch>
					<Route exact path='/' component={Home}/>
					{/* <Route path='/about' render={(props) => ( <MarkDowner contentType={"core"} {...props} /> ) }/> */}
					<Route path='/about' component={MarkDowner} />
					<Route path='/workshop' component={Workshop}/>
					<Route path='/projects/:project' component={MarkDowner} />
				</Switch>
			</div>
		);
	}
}

export default App