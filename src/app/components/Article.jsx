import React from 'react'
import '../styles/Article.scss'

const Article = (props) => {
	console.log(props);
	return (
		<div className="row article">
			<div className="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-offset-3 col-md-6">
				<article>
					<h1>{props.title}</h1>
					<div className="article-body" dangerouslySetInnerHTML={{__html: props.content}}></div>
				</article>
			</div>
		</div>
	);
}

export default Article