import React from 'react'
import { Link } from 'react-router-dom'
import marked from "marked";
import projectData from '../content/projects/index.js'
import Article from './Article.jsx'

// Get the slug from request,
// lookup the relevant md file for slug,
// render content

class ArticleContainer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {project: projectData[0]};
	}

	getContentFile(projectData, slug) {

		return new Promise(function(resolve, reject) {
			projectData.map((item) => {
				if (item.slug === slug) {
					resolve(item);
				}
			})
		});
	}

	componentWillMount() {
		this.getContentFile(projectData, this.props.match.params.slug)
		.then(project => {
			const content = require("../content/projects/" + project.contentFile);
			this.setState({project: project, content: content}, this.loadContent) 
		})
	}

	loadContent() {
		fetch(this.state.content)
		.then(response => {
			return response.text()
		})
		.then(text => {
			this.setState({
				markdown: marked(text)
			})
		})
	}

	render () {
		const { markdown } = this.state;
		return(
			<Article title={this.state.project.title} content={ markdown } />
		);
	}
}

export default ArticleContainer