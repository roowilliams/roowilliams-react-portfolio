import React from 'react'
import { Link } from 'react-router-dom'
import '../styles/Button.scss'

class Button extends React.Component {
	render () {

		return(
				<div className="btn">
					<Link to={this.props.link}>{this.props.text}</Link>
				</div>
			);
	}
}

export default Button