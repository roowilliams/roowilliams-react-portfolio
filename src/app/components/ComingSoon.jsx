import React from 'react'
import '../styles/ComingSoon.scss'

class ComingSoon extends React.Component {
	render () {
		return (
			<h1 className="coming-soon">Coming Soon</h1>
		);
	}
}

export default ComingSoon