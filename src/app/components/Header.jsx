import React from 'react'
import '../styles/Header.scss'
import Logo from './Logo.jsx'
import Nav from './Nav.jsx'
import Intro from './Intro.jsx'

class Header extends React.Component {

	render() {
		return (
			<div className="header">
			<div className="row">
				<div className="col-xs-10 col-xs-offset-1 col-md-5">
					<Logo />
				</div>
				<div className="col-xs-4 col-xs-offset-1">
					<Nav />
				</div>
			</div>
			</div>
		);
	}
}

export default Header;