import React from 'react'
import Intro from './Intro.jsx'
import ProjectList from './ProjectList.jsx'

class Home extends React.Component {
	render () {
		return (
			<div>
				<Intro />
				<ProjectList />
			</div>
		);
	}
}

export default Home