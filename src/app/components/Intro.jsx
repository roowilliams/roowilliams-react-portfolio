import React from 'react'
import '../styles/Intro.scss'

class Intro extends React.Component {
	render () {
		return(
			<div className="row intro">
				<div className="col-xs-offset-1 col-xs-10 col-sm-8 col-md-6 col-lg-6">
					<p>
						Hello. I am an Interaction Designer and UI Engineer 
						with a passion for making products and experiences at 
						the intersection of our physical and digital worlds.
					</p>
				</div>
				<div className="overlay"></div>
			</div>
		);
	}
}

export default Intro