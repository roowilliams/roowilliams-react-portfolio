import React from 'react';
import '../styles/Logo.scss'

class Logo extends React.Component {

	render() {
		return (
				<h1 className="logo">
					<a href="/">
						Roo Williams
					</a>
				</h1>
		);	
	}
}

export default Logo;