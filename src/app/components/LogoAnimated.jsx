import React from 'react';
import '../styles/LogoAnimated.scss'

class Logo extends React.Component {

	componentDidMount() {

			// Clone logo element to create layered animation effect
			var node = document.getElementsByClassName('neon')[0];

			var parent = document.getElementsByClassName('text')[0];

			for (var i = 0; i < 3; i++) {
				var dupNode = node.cloneNode(true);
				parent.prepend(dupNode);
			}
		}

	render() {
		return (
			<div className="glitch">
				<div className="text">
				<h1>
				<a href="/">
				<span className="neon">
				Roo Williams
				</span>
				</a>
				</h1>
				</div>
			</div>
		);	
	}
}

export default Logo;