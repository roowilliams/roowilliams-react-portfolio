import React from 'react'
import { Link } from 'react-router-dom'
import '../styles/Article.scss'
import marked from "marked";
import Article from './Article.jsx'

// import config from "../config/index.js"

// Get the slug from request,
// lookup the relevant md file for slug,
// render content

class MarkDowner extends React.Component {

	constructor(props) {
		super(props);
		this.state = {}
	}

	componentWillMount() {
		this.update(this.props);
	}	

	update(props) {
		const content = props.match.url;
		const md = require("../content" + props.match.url + ".md");
		this.setState({content: content, md: md}, this.loadContent) 
	}

	componentWillReceiveProps(nextProps) {
		this.update(nextProps);
	}

	loadContent() {
		fetch(this.state.md)
		.then(response => {
			return response.text()
		})
		.then(text => {
			this.setState({
				markdown: marked(text)
			})
		})
	}

	render () {
		const { markdown } = this.state;
		return(
			<Article title={this.state.content.title} content={markdown} />
		);
	}
}

export default MarkDowner