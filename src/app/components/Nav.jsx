import React from 'react';
import { Link } from 'react-router-dom'
import '../styles/Nav.scss'

class Nav extends React.Component {

	// Close nav on navigation
	closeNav () {
		document.getElementById('nav-trigger').checked = false;
	}

	render() {
		return (
			<nav>
				<input type="checkbox" id="nav-trigger" className="nav-trigger" />
				<label htmlFor="nav-trigger"></label>
				<ul className="navigation">
					<li><Link to='/' onClick={this.closeNav}>Home</Link></li>
					<li><Link to='/about' onClick={this.closeNav}>About</Link></li>
					<li><Link to='/workshop' onClick={this.closeNav}>Workshop</Link></li>
					<li><a href='http://roowilliams.com/pdf/RooWilliams-CV-2017.pdf' target="_blank" onClick={this.closeNav}>Resume</a></li>
				</ul>
			</nav>
		)
	}
}

export default Nav;