import React from 'react'
import { Link } from 'react-router-dom'
import '../styles/Project.scss'
import Button from './Button.jsx'

class Project extends React.Component {
	render () {

		return(
				<div className="row project middle-md">
					<div className={"col-xs-offset-1 col-xs-10 col-md-4 last-xs "  + (this.props.layout || '')}>
						<Link to={`projects/${this.props.project.slug}`}><h2>{this.props.project.title}</h2></Link>
						<p>{this.props.project.blurb}</p>
						<Button link={`projects/${this.props.project.slug}`} text="View" />
					</div>
					<div className="image col-xs-offset-1 col-md-5 col-xs-10">
						<Link to={`projects/${this.props.project.slug}`} className="imagelink">
							<img src={this.props.project.image} />
						</Link>
					</div>
				</div>
			);
	}
}

export default Project