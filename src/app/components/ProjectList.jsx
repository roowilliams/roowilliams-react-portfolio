import React from 'react'
import '../styles/ProjectList.scss'
import projectData from '../content/projects/index.js'
import Project from './Project.jsx'

class ProjectList extends React.Component {
	render () {
		
		// Programmatic layout, could also be achieved with css odd/even selectors
		// at the expense of not using grid layout classes.

		var projects = projectData.map((project, index) => {
			var layout;
			if (index % 2) {
				layout = "first-md"
			}
			return <li key={index}><Project project={project} layout={layout} /></li>
		});

		return(
			<div className="row projects">
				<ul className="col-xs-12 project-list">
					{projects}
				</ul>
			</div>
		);
	}
}

export default ProjectList