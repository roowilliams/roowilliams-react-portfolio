import React from 'react'
import '../styles/Spinner.scss'

class Spinner extends React.Component {
	render () {
		return (
			<div className='spinner'></div>
		);
	}
}

export default Spinner