import React from 'react'
import Spinner from './Spinner.jsx'
import ComingSoon from './ComingSoon.jsx'

class Workshop extends React.Component {
	render () {
		return (
			<div>
				<ComingSoon />
				<Spinner />
			</div>
		);
	}
}

export default Workshop