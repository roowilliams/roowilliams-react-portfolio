---
name: Winston the Bulldog
contact:
  email: dogsdontuseemail@hzdg.com
---

![Roo Williams][1]


> Fueled by a burning curiosity and desire to make tomorrow better than today, I strive to create products and experiences that leverage the possibilities unlocked by a networked world.

T-shaped person with a broad range of skills from strategy to production, technical to creative, I am solutions-oriented in any context. A naturally curious mind that is able to harness creative energy. An innate ability to deliver laser focused, succinct insights plus the vision and skills to execute great work at any level of fidelity. Able to generate clear vision for teams and confident enough to lead from the bottom up.

I live in Brooklyn, New York with my partner <a href="https://www.instagram.com/lydia_pang_/" target="_blank">Lydia</a>, and our tiny dog <a href="https://www.instagram.com/bettyjudgesyou/" target="_blank">Betty</a>.

[1]: /img/roowilliams.jpg#center
