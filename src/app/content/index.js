export default [
	{
		"title": "About",
		"path": "/about",
		"contentFile": "about.md"
	},
	{
		"title": "Workshop",
		"path": "/workshop",
		"contentFile": "workshop.md"
	}
];