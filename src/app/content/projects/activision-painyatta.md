![][1]

When I was asked to build a robotic arm that would hit a pinata at an event full of children in just over a week I knew that the first thing I had to do was sketch out how it might look. The second thing I needed to do was call [Tim at ELK](http://eastlondonkinetics.co.uk/) and ask for his help!

![](/img/portfolio/painyatta/Activision-Skylanders_Launch-Painyatta-Digital-Sketch.jpg)



To promote the new Skylanders game launch Activision were hosting an event at the Royal Festival Hall, London. To bring the launch event to life digitally TMW proposed using Painyatta (one of the main characters from the game) as a physical Pinata that could be hit by anyone tweeting to a #HitPainyatta hashtag. The prospective tweeters could watch the event via a livestream and any prizes knocked out of the pinata by their tweet would be sent to them.

### Case Study Video
<video width="100%" preload="auto" controls> 
  <source src="/img/portfolio/painyatta/Painyatta.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

The plan was to build an app that would live capture all the tweets so I enlisted [Ciaran Park](http://www.ciaranpark.com) to create the Node.js app. A moderator would check the tweets to make sure they weren’t offensive and if OK the tweet would be sent to a LED ticker and at the same time cause the baseball bat to swing via a very large Arduino controlled stepper motor.

I took the opportunity to learn more about stepper motors so volunteered to focus on the electrical and programming side of things whilst Tim and his team tackled the physical build.

<div class="gallery-grid">
<div class="row">

<div class="col-xs-6 col-sm-3">
<img src="/img/portfolio/painyatta/BADASS_LED_Ticker.gif" alt="" />
</div>
<div class="col-xs-6 col-sm-3">
	<img src="/img/portfolio/painyatta/Stepper_Test_2.gif" alt="" />
</div>

<div class="col-xs-6 col-sm-3">
<img src="/img/portfolio/painyatta/Baseball_Bat_Test.gif" alt="" />
</div>
<div class="col-xs-6 col-sm-3">
<img src="/img/portfolio/painyatta/Baseball_Bat_Spray.gif" alt="" />
</div>

</div>
</div>

![](/img/portfolio/painyatta/Stepper_Motor_Column_With_Bat.jpg#center)

![](/img/portfolio/painyatta/Activision-Skylanders_Launch-Painyatta-Digital-Event-Truss.jpg#center)

![](/img/portfolio/painyatta/Activision-Skylanders_Launch-Painyatta-Digital-Event.jpg#center)

![](/img/portfolio/painyatta/Activision-Skylanders_Launch-Painyatta-Digital-Event-Direction.jpg#center)

![](/img/portfolio/painyatta/Activision-Skylanders_Launch-Painyatta-Digital-Event-End.jpg)

## The Result

The event was a great success with thousands of tweets recieved during the 3 hour event and the live stream coverage watched by hundreds online.

![](/img/portfolio/painyatta/Activision-Skylanders_Launch-Painyatta-Digital-Event-Tweets.jpg#center)


[1]: /img/portfolio/painyatta/Activision-Skylanders_Launch-Painyatta-Digital-Event-Full.jpg