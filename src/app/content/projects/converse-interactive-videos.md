+++
image = "/img/portfolio/converse-wetropolis/featured-image.gif"
showonlyimage = false
date = "2014-10-05T19:44:32+05:30"
title = "Converse Interactive Videos"
draft = false
weight = 5
blurb = "Interactive music videos for Converse product launch and Chuck II wear test."
tech = [ 
	'HTML5/CSS3',
	'Javascript',
	'getUserMedia API'
]

[[collaborators]]
	name = 'Pete Jostrand'
	url = 'https://www.peterjostrand.com/'

[[collaborators]]
	name = 'Josh Chua'
	url = 'https://www.josh-chua.com/'

[[collaborators]]
	name = 'Ben Dean'
	url = 'http://www.bendean.com.au/'

[[collaborators]]
	name = 'Linda Yang'
	url = 'http://lindayang.com/'

[[links]]
	name = 'Converse'
	url = 'http://counterclimate.converse.com'

[[links]]
	name = 'Hypebeast'
	url = 'https://hypebeast.com/2016/8/keith-ape-converse-diamonds-jedi-p'

[[links]]
	name = 'Creativity Online'
	url = 'http://creativity-online.com/work/converse-counter-climate-keith-ape-interactive-video/48617'

[[links]]
	name = 'Awwwards SOTD'
	url = 'https://www.awwwards.com/sites/converse-diamonds'
	
+++

### Wetropolis
To launch Converse's first-ever water-resistant Chucks I helped out creative team <a href="https://www.josh-chua.com/" target="_blank">Josh Chua</a> and <a href="https://www.peterjostrand.com/" target="_blank">Pete Jostrand</a> by building prototypes to test interactions, aid creative development and sell in ideas for Converse, ultimately leading to an interactive music video performed by Keith Ape.

{{< youtube UjlTEB63iqU >}}

<br>
<br>

### H09909

Working with creative team <a href="http://www.bendean.com.au/" target="_blank">Ben Dean</a> and <a href="http://lindayang.com/" target="_blank">Linda Yang</a>, I produced a prototype to test ability to detect light levels in a room via a user's webcam and swap out content displayed accordingly. This was used to help the client buy the idea with confience.

The creative team worked with H09909 to produce two versions of a music video.

Using the technology we prototyped allowed someone to switch between a version of the video shot in light whilst watching in a lit room, and a darker, scarier version of the video by turning out the light.

{{< vimeo 157636080 >}}