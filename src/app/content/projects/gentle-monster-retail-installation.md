+++
date = "2016-11-05T19:41:01+05:30"
title = "Gentle Monster Retail Installation"
draft = false
image = "img/portfolio/gentle-monster/gentle-monster-featured.jpg"
showonlyimage = false
weight = 4
blurb = "Bringing a static sculpture to life."
tech = [ 
	'HTML5/CSS3',
	'Javascript'
]

[[links]]
	name = 'Observer'
	url = 'http://observer.com/2016/07/i-love-unique-retail-concepts-like-korean-eyewear-brand-gentle-monsters-ny-flagship/'

[[links]]
	name = 'Hypebeast'
	url = 'https://hypebeast.com/2016/5/gentle-monster-grand-blue-installation'

[[links]]
	name = 'High Snobiety'
	url = 'https://www.highsnobiety.com/2016/06/01/gentle-monster-grand-bleu-installation-ny-flagship/'

[[links]]
	name = 'Sidewalk Hustle'
	url = 'https://sidewalkhustle.com/gentle-monster-nyc-unveils-grand-blue-installation-in-nyc/'
	
+++


![Gentle Monster Installation](/img/portfolio/gentle-monster/gentle-monster-featured.jpg)

I provided technical and installation assistance with a custom projection to help bring a static sculpture to life with rippling light using multiple projectors at Gentle Monster's NYC flagship store. Full screen, looping video player made with HTML and JavaScript.

![Gentle Monster Installation](/img/portfolio/gentle-monster/gentle-monster-grand-bleu-installation-ny-flagship-01.jpg)

![Gentle Monster Installation](/img/portfolio/gentle-monster/gentle-monster-grand-bleu-installation-ny-flagship-02.jpg)


![Gentle Monster Installation](/img/portfolio/gentle-monster/gentle-monster-installation-video.gif#center)


![Gentle Monster Installation](/img/portfolio/gentle-monster/gentle-monster-grand-bleu-installation-ny-flagship-03.jpg)

![Gentle Monster Installation - Work in Progress](/img/portfolio/gentle-monster/gentle-monster-wip.jpg)


*Photos courtesy of Gentle Monster & Sidewalk Hustle*