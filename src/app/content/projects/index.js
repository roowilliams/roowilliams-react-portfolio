export default [
	{
		"title": "Activision Painyatta Live Event Digital Activation",
		"blurb": "A tweet-powered baseball bat to bring a digital presence to the launch event of Activision's Skylanders Trapteam.",
		"date": "01/03/2015",
		"image": "img/project1.jpg",
		"slug": "activision-painyatta",
		"contentFile": "activision-painyatta.md"
	},
	{
		"title": "Are You Better Than Our Worst?",
		"blurb": "Recruiting interns to put pressure on the worst employees at the best agency.",
		"date": "01/03/2015",
		"image": "img/project2.jpg",
		"slug": "anomaly-areyoubetterthanourworst",
		"contentFile": "anomaly-areyoubetterthanourworst.md"
	}
];