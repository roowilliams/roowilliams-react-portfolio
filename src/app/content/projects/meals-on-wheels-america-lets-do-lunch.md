+++
showonlyimage = false
draft = false
image = "img/portfolio/meals-on-wheels-americaletsdolunch/featured-image.jpg"
date = "2016-11-05T18:25:22+05:30"
title = "Meals on Wheels America: Let's Do Lunch"
weight = 5
blurb = "A project to help Meals on Wheels recruit new volunteers."
tech = [ 
	'HTML5/CSS3',
	'Bootstrap',
	'Javascript',
	'Google Maps API',
	'SendGrid',
	'Ruby on Rails',
	'Redis',
	'Nginx'
]

[[collaborators]]
	name = 'Chris Allick'
	url = 'http://chrisallick.com/'

[[collaborators]]
	name = 'Jake Doran'
	url = 'http://www.jakedoran.me/'

[[collaborators]]
	name = 'Adriana Longoria'
	url = 'http://adrianalongoria.com/'

[[links]]
	name = "America Let's Do Lunch"
	url = 'http://americaletsdolunch.com'

[[links]]
	name = 'Mashable'
	url = 'http://mashable.com/2016/07/16/meals-on-wheels-lets-do-lunch/#retEykkBrSqG'

[[links]]
	name = 'Adweek'
	url = 'http://www.adweek.com/creativity/these-poignant-ads-show-why-you-should-be-doing-lunch-american-seniors-172470/'

[[links]]
	name = 'Shorty Awards'
	url = 'http://shortyawards.com/9th/meals-on-wheels-america-lets-do-lunch-volunteer-recruitment-campaign'

+++

Meals on Wheels America is a complex organization comprised of over 5000 fiercely independent programs servicing their local area's seniors.

They approached Anomaly to ask for help recruiting new volunteers. We developed a creative messaging campaign to try and shift negative perceptions of older people.

{{< youtube 9R3dQAq6SfY >}}


But even on the back of a successful, emotionally motivating campaign, there was still a huge problem to solve.

### Challenges

One glance at a volunteer signup form was enough to make someone's eyes glaze over, and when combined with the vetting process that must take place for all volunteer prospects, it was plain to see why it was a struggle to convert a prospect into a qualified volunteer.

<a href="/img/portfolio/meals-on-wheels-americaletsdolunch/volunteer-form.jpg" data-toggle="lightbox">
	<img src="/img/portfolio/meals-on-wheels-americaletsdolunch/volunteer-form.jpg#center" class="small-image" />
</a>


Within our scope we weren't able to affect the vetting procedures, but delving into the existing signup processes, it turned out that most local programs had their own ways of signing up from forms to telephone and email. Meals on Wheels didn't necessarily rise to the top of SEO rankings, especially with localized results so each prospect had to work to find their local program. Delving deeper and looking at a few of these, we found that between each, there was little consistency in the way they were organized, the format of their premises, branding, volunteering opportunities or sign up process.

We were creating ads to deliver a nation-wide message that worked for all programs, and so we needed a single, straight forward call to action that we could use to let people activate themselves should our messaging be successful.

### The Solution 

Our strategy was to create a straight forward signup site that would match a prospective volunteer with the program most local to them. Because of the amount of information required to become a volunteer, our target outcome was to get the prospect on the phone with the local program as soon as possible. That way they could work the details out with a human and the personal connection would carry their motivation through the vetting process. Guided by that philosophy we aimed to create a site that offered minimal distraction, and a signup form that was as frictionless as possible.

Internally, within Meals on Wheels America, work was being done to educate the programs on what the site would enable and prepare them to convert prospects. Each program had different levels of IT proficiency, but they were all required to have an email address.

We decided to use this as the delivery mechanism for new volunteer prospects to each local program. Using a cron job, once a day our web application would email a list of new matches to the email on file for volunteer recruitment at the local program via the SendGrid API. All the person manning the inbox had to do was go through and call each prospect to start the recruitment process.



Our starting point was to create the match, so we needed to know where people were located. Meals on Wheels has a lot of volunteers that are retirees, which makes sense as the bulk of volunteering opportunities lie in delivering warm meals at lunchtime. Our campaign aimed to target people in employment too. Out of that aim came a challenge; working people that are happy to help during lunchtimes are probably located somewhere different to their home address. We didn't want to add additional questions for each persona, because we didn't want to increase cognitive load and cause people to drop off.

I put my interaction design head on and leveraged the Google maps API, mocking up different location finding formats. After testing and iterating on as many people as possible, modeling various scenarios across multiple devices I ended up with an interaction that instantly displayed your location on entry of a 5 digit zip code, then highlighted a draggable marker to enable people to select their preferred volunteering location. They started by inputting their home zip, then once they saw a map of the location they were familiar with, had the ability to drag the marker to their preferred volunteering location.

![Meals on Wheels - America Let's Do Lunch Site](/img/portfolio/meals-on-wheels-americaletsdolunch/form-location.gif#center)

Our form design took cues from Google's Material Design. I chose to keep the speed on the animations high to make filling it in feel quick and responsive.

![Meals on Wheels - America Let's Do Lunch Site](/img/portfolio/meals-on-wheels-americaletsdolunch/form-filling-in-submission.gif#center)

People like other people to know when they do something good, so after signup we offered our prospects the ability to share the 'lunch flag' socially in the hope that this social activity would help bring more prospects to the site.

From a technical perspective, on submission a request is sent off to a SOAP service which performs the matching, and the correct local program is returned as an XML object. We did work on the backend to convert that to JSON which was then passed to the front end as an object. We also sent a 'thank you' email to our prospects with the details of their matched program so they could choose to reach out of them if need be. Furthermore, we later added a followup survey to be sent 6 weeks later to see how their volunteering experience went.

What sounded at the outset like a simple site build turned into more of a service design project comprising of back stage and front stage operations, multiple channels and touch points. We were fortunate to build a very good relationship with the Meals on Wheels team and worked together to develop both parts together, in tandem to create a successful, end-to-end campaign and a simple, effective prospect recruiting system.

<a href="https://americaletsdolunch.org/" target="_blank">AmericaLetsDoLunch.org</a>

![Meals on Wheels - America Let's Do Lunch Site](/img/portfolio/meals-on-wheels-americaletsdolunch/Americaletsdolunch.gif#center)

In just over a year the site has gathered over 63,000 signups and conversion rate is way above average. In fact, the client was so happy that Meals on Wheels have changed their main signup form to point to americaletsdolunch.org.

Personally, this project was a great learning experience for me. It started with myself and <a href="http://chrisallick.com/" target="_blank">Chris Allick</a> as the dev team. Chris implemented a back end stack of Ruby on Rails, Redis Passenger and Nginx whereas I tackled the system design, interactions design and front-end engineering. Midway through the project Chris left Anomaly to pursue other ventures so I took the opportunity to learn Ruby on Rails and Nginx to complete and manage the remainder of the project. Keeping a pro-bono project lean I worked directly with the client to sell in ideas and with external vendors and APIs, whilst managing production of the project, all aspects of development, server provisioning, set up and deployment. Phew!

When the Trump administration announced budget plans that would severely impact Meals on Wheels, we came up with another idea to help them push back: [Savelunch](http://localhost:1313/portfolio/meals-on-wheels-savelunch/).