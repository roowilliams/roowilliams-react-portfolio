+++
image = "/img/portfolio/painyatta/Activision-Skylanders_Launch-Painyatta-Digital-Event-End.jpg"
showonlyimage = false
date = "2014-10-05"
title = "MetaDerm Skin Companion"
draft = false
weight = 3
blurb = "A digital companion to help skin condition sufferers track progress."
tech = [ 
	'Node.js',
	'React Native'
]

[[collaborators]]
	name = 'Liza Winton'
	url = 'https://www.linkedin.com/in/elizabethwinton/'

[[collaborators]]
	name = 'Jake Doran'
	url = 'http://www.jakedoran.me/'

[[collaborators]]
	name = 'Justin Tang'
	url = 'http://justintang.us'
	
+++

Skin conditions are often affected by a number of factors including dietary, sleep and stress levels. Skin-journalling is a common way for people suffering with conditions to spot anything that could cause their condition to flare up by monitoring these factors and comparing it to the state of their condition. When MetaDerm, a herbal skincare product designed to treat and prevent inflamatory skin conditions such as psoriosis and eczema approached Anomaly with the desire to grow, I pitched the idea of creating a companion app to go alongside their core product.

### Why An App?

I found out that a user would often need to comply with applying the product consistently over a time period of 4-6 weeks before seeing any results and because the product is made with natural ingredients, it was safe to do so.
Knowing that the product wouldn't deliver instantaneous results, I felt an app would help the consumer in the following ways:

- Creates an aspirational course that the user can enrol on, which feels different to any competitor products.
- Allow the user to genuinely solve the problem by spotting correlations in the data between physiological and external factors.
- Show the consumer that we don't just care about selling you the product, but we actually care about results.
- Opens up a channel for MetaDerm to speak to users, test and learn, one of the fundamental principles startups use for developing products and growing.
- Build a strong community around the product. In a category where word of mouth, and before/after pictures are king, it's a good idea to try and harness the success stories of the community around the product.
- Act as a peer to peer marketing tool, helping to generate stories that bloggers and influencers would want to talk about offering another way in to the product purchase funnel.
- And long-term, by aggregating user data and creating an analysis engine in the cloud, act as a leading authority on what causes flare ups, use it as part of a content strategy and to reveal future opportunities for product innovation. 

### Prototype

<a href="/img/portfolio/metaderm-skin-companion/prototype-screens.jpg" data-toggle="lightbox">
![](/img/portfolio/metaderm-skin-companion/prototype-screens.jpg#center)
</a>

I built the initial prototype using a Javascript stack. It was rough and ready, everything you'd expect in a pitch!
During the pitch, I was able to take the client through the prototype on a mobile device and actually put it in their hands so they could try it out for themselves. Buttons that did nothing served as props to talk about speculative features, which prompted lots of interesting discussion.
This contributed to a winning pitch and a few months after establishing our working relationship we were allocated a small budget and a 6 week timeframe to develop a version of the app that could be tested with a small sample of real users.

### A Better Prototype

We had a rough estimate that the devices owned by the users would be around a 50/50 split of iOS and Android. I made the decision lead development of our app using React Native as apart from a few tweaks to make code native to each OS, this would allow us to deploy to both mobile operating systems using about 95% of the same codebase. Due to the short time-frame, and other projects running in tandem, rather than attempting to learn React Native myself I found a developer we could bring onboard to get the app developed.

Again, due to time constraints, we would mostly be relying on pre-built React Native components and libraries to bring our app to life, so while we were able to specify loosely what kind of interactions we thought the app needed, we weren't able to create exactly what we had envisioned for the user.
We worked as a small team in sprints.
We started by brainstorming and prioritizing features, before articulating them into user stories.

<a href="/img/portfolio/metaderm-skin-companion/brainstorming-features.jpg" data-toggle="lightbox">
![](/img/portfolio/metaderm-skin-companion/brainstorming-features.jpg#center)
</a>

We used group whiteboarding sessions to plan our app's interaction models, and the wider flow of engagement.

<a href="/img/portfolio/metaderm-skin-companion/whiteboarding-session.jpg" data-toggle="lightbox">
![](/img/portfolio/metaderm-skin-companion/whiteboarding-session.jpg#center)
</a>

<a href="/img/portfolio/metaderm-skin-companion/app-flow-1.png" data-toggle="lightbox">
![](/img/portfolio/metaderm-skin-companion/app-flow-1.png#center)
</a>

And after wireframing, built an InVision prototype to test out the flow of the app and communicate with the client.

[InVision here]

![](/img/portfolio/metaderm-skin-companion/in-use-1.jpg#center)

#### Challenges

Our main challenges came in the form of deciding what language to use. This was particularly prevelant when trying to create a simple interface for people to report 
