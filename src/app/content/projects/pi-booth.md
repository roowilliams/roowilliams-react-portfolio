+++
draft = false
image = "img/portfolio/pibooth/Pi-Booth-Smart-Phone-Interface.jpg"
showonlyimage = false
date = "2014-07-16T20:22:08+05:30"
title = "Pi-Booth"
weight = 4
blurb = "A photobooth powered by Raspberry Pi."
tech = [
	'Raspberry Pi',
	'3D printing',
	'Laser cutting',
	'Python',
	'Flask',
	'Socket.io',
	'HTML5/CSS3',
	'Javascript',
]

[[links]]
	name = 'Hackaday'
	url = 'https://hackaday.com/2015/06/05/smile-for-the-raspberry-pi-powered-photo-booth/'

[[links]]
	name = '3Dprint.com'
	url = 'https://3dprint.com/71862/pi-booth-photo-booth/'

[[links]]
	name = 'Electronics Weekly'
	url = 'https://www.electronicsweekly.com/market-sectors/embedded-systems/developer-makes-raspberry-pi-photo-booth-2015-06/'

+++

![](/img/portfolio/pibooth/Pi-Booth-Raspberry-Pi-Photo-Booth.jpg#center)

### Context
It’s the first week of your new job. You are invited to an induction session, so you step into an unfamilliar room full of new people you will be spending the majority of your time with for the foreseeable future. The HR person pulls out a compact camera and takes aim at you you whilst everyone watches. You suddenly become aware of your gummy smile. You don’t want to smile, but you must. You feel yourself going cockeyed. The flash goes off. You know it’s bad. You want to go again but can’t risk looking vain. You die inside and laugh it off as the camera prays on the next victim.

Of course, you later find out will serve as an avatar attached to every email you send and every intranet post you make for the rest of your days at the agency.

I was tasked with improving this and given free reign to execute it how I wanted.

### The Result
The result was a mobile light box with camera controlled entirely via the user’s smartphone. The device interfaces with the IT system to allow any employee to automatically update their photo at any time.

![](/img/portfolio/pibooth/portraits.gif#center)

<video width="100%" preload="auto" loop="loop" autoplay muted> 
  <source src="/img/portfolio/pibooth/Pi-Booth-Usage.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

### Details
The device is around a Raspberry Pi running a Python application.
Flask, the webserver module is combined with Flask-Socketio to provide a web interface and controller that accesses the Raspberry Pi camera. Using the GPIO pins to control a relay module hooked up to 2 strip lights I was able to create a timed bulb flash.

![](/img/portfolio/pibooth/Pi-Booth-Raspberry-Pi-Relay.jpg)

The unit was designed and built to be fabricated using digital manufacturing technologies; flat laser cut pieces are combined with 3D printed parts.

![](/img/portfolio/pibooth/Pi-Booth-Laser_Cut_Pieces.jpg)

![](/img/portfolio/pibooth/Pi-Booth-Process.jpg)

![](/img/portfolio/pibooth/Pi-Booth-Process-Internals.jpg)

By removing the stock lens from the Raspberry Pi’s camera module and fitting it with a 3D printed adaptor I created a way to exchange lenses giving the booth a greater field of view and options for different shooting scenarios. I also added a button to turn the screen on and off.

![](/img/portfolio/pibooth/raspberry-pi-camera-module-board.jpeg)

![](/img/portfolio/pibooth/Pi-Booth-Raspberry-Pi-Photo-Booth-Camera.jpg)

Smartphone Controlling Physical Products
The system needed a way for a user to input their username so that the resulting photo could be linked to their email. I could have used a touch screen or added a keyboard but thought that this was an opportunity to test a new way to interface with physical products.

The booth is controlled entirely through a smartphone, via a website. There is something quite fun about pressing a digital button on a smartphone and getting physical feedback (in the form of the flash). This also created an inspiration/talking point, eg. could people interact with a brand object or installation in a shop window via their phones? What could be the mechanic that leads up to that control? What other devices could we control in this way? Will future generations use smartphones more than they use dials and pushbuttons?

![](/img/portfolio/pibooth/Pi-Booth-Flash.jpg)

Smartphone as democratic remote control still fascinates me.