+++
draft = false
image = "img/portfolio/sentimeter/Sentimeter.jpg"
showonlyimage = false
date = "2016-11-05T19:50:47+05:30"
title = "Diageo Sentimeter v1 & v2"
weight = 10
blurb = "A novel data collection tool to measure the mood of the workplace."
tech = [ 
	'Arduino UNO / YUN',
	'HTML5/CSS3',
	'Javascript',
]

[[collaborators]]
	name = 'Ash Nolan'
	url = 'https://ashleynolan.co.uk/'

+++

![](/img/portfolio/sentimeter/Sentimeter.jpg#center)

At TMW I ran a fortnightly ‘Hack Club’ where the geeks get together to nerd out and talk tech. We were asked if we could help promote the Data department’s role within the agency and also make people more aware of what data actually is. The result was the Sentimeter, a device that collects data around the simple question of ‘How are you feeling?’.

<video width="100%" preload="auto" loop="loop" autoplay muted> 
  <source src="/img/portfolio/sentimeter/Sentimeter.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


### Version 1

![](/img/portfolio/sentimeter/Sentimeter-Button-Press.jpg#center)

The data displayed on a large screen near the device updates instantly providing a satisfying moment for the user, no matter how they’re feeling. Each press is collected along with the time, date and current weather information into a SQL database.

![](/img/portfolio/sentimeter/Sentimeter-Data-Screen-Context.jpg#center)

![](/img/portfolio/sentimeter/Sentimeter-Data-Display.jpg#center)

![](/img/portfolio/sentimeter/Sentimeter-CAD-Animation.gif#center)

I initally experimented with RFID as a way to get a much richer dataset, the RFID tags providing the ability to track individual cards but experienced inconsistent results with RFID signal strength and the card readers we were using. Read more about that in this blog post.

![](/img/portfolio/sentimeter/Sentimeter-Process-Arduino-3D-Printing.jpg#center)

One of the most interesting parts of this project is the user behaviour that emerged. Results are easily skewed by constantly tapping on one of the options and some people like to game the system. Then there are users that like to game the opposite way resulting in a statistical tug-of-war. When the Data department conducted their analysis to tease out some stories we had to strip out the taps that came in within 2 seconds of each other.

![](/img/portfolio/sentimeter/Sentimeter-Data-Analysis.jpg#center)

Paper prototypes allowed me to understand how people might use the proposed Sentimeter and what emotions they might like to show. After some discussion the decision was made to only include two options to force people to polarise and think about how they felt.

### Version 2

Inspired by Sentimeter v1 Diageo’s Media Futures department commissioned us to create a Sentimeter for them. It needed to be robust enough to deploy at a client’s office without requiring maintenance.

I decided to built it on the Arduino Yun platform, which is a WiFi-enabled Arduino that runs a tiny distribution of Linux. The Arduino would take care of the tap interactions and the Linux part would host a SQLite database that would handle the questions and answers.

The unit broadcasts its own WiFi network. To see the results all you’d need to do is connect to the WiFi network and visit a page hosted on the Yun’s built in webserver.

I decided to use capacitive touch points for the options in this version due to learnings from the previous. In v1 the mechanical touch pads and buttons would eventually fail - using capacitive touch would mean that there would be no mechnical issues.

![](/img/portfolio/sentimeter/diageo-logo.png#center)

<div class="gallery-grid">
<div class="row">

<div class="col-xs-12 col-sm-6">
<img src="/img/portfolio/sentimeter/Sentimeter_v2-Tap.gif" alt="" />
</div>
<div class="col-xs-12 col-sm-6">
<img src="/img/portfolio/sentimeter/Sentimeter_v2-Stats.gif" alt="" />
</div>

</div>
</div>

![](/img/portfolio/sentimeter/Sentimeter_v2.jpg#center)

![](/img/portfolio/sentimeter/Sentimeter_v2-Build.jpg#center)

![](/img/portfolio/sentimeter/Sentimeter_v2-Process.gif#center)