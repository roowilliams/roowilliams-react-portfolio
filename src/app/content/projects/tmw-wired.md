+++
draft = false
image = "img/portfolio/tmw-wired/TMW-Wired-Steady-Hand-Game-2.jpg"
showonlyimage = false
date = "2013-07-16"
title = "TMW Wired"
weight = 9
blurb = "A huge, connected steady hand game to add a buzz to live events."
tech = [ 
	'Arduino',
	'RFID',
	'3D Printing',
	'Node.js',
	'Twitter API'
]

+++


In 2013 TMW Unlimited packed up shop on the King's Road, Chelsea and shipped over to a brand new Soho premesis.
We wanted to show our new home off to our clients by throwing a house-warming party. We also thought it'd be a good opportunity to show off some of the tech we'd been playing with in the form of a connected giant steady hand game.

We initially sent an email invite out to our clients that asked them to RSVP with their Twitter handle. Once we had their Twitter handle we linked each attendee to a RFID bracelet.

<img src="/img/portfolio/tmw-wired/TMW-Wired-RFID-Bracelet.jpg#center" title="TMW Wired: a huge, connected steady hand game" alt="TMW Wired: a huge, connected steady hand game"/>

On the evening of the party each attendee was given their wristband. Tapping this wristband against an RFID reader mounted on the game let the system know who was playing.

The user's progress around the course was tracked using a webcam which scrubbed through a video tour of our new office on a projected image alongside the game. It quickly became apparent that people weren't engaged by the video and it was all about watching the person playing!

<img src="/img/portfolio/tmw-wired/TMW-Wired-Steady-Hand-Game-2.jpg#center" title="TMW Wired: a huge, connected steady hand game" alt="TMW Wired: a huge, connected steady hand game"/>

 As you try to run the course faster, the skill-level required rises making this activity perfect for inducing flow state. This made it all the more jarring when the loop was touched. Should you fail this merciless game you'd be getting a blast from a klaxon horn, coupled with a vibration in the loop handle producing an effect that actually felt like an electric shock.

<img src="/img/portfolio/tmw-wired/TMW-Wired-Loop-Vibration-Motor.jpg#center" title="TMW Wired: a huge, connected steady hand game" alt="TMW Wired: a huge, connected steady hand game"/>

And if that wasn't enough, a webcam captured that shocking moment and tweeted it, publicly.

<img src="/img/portfolio/tmw-wired/TMW-Wired-Steady-Hand-Game-1.jpg#center" title="TMW Wired: a huge, connected steady hand game" alt="TMW Wired: a huge, connected steady hand game"/>

<img src="/img/portfolio/tmw-wired/TMW-Wired-Steady-Hand-Game-4.jpg#center" title="TMW Wired: a huge, connected steady hand game" alt="TMW Wired: a huge, connected steady hand game"/>

I was tasked with the complete design and build. I learned how to cut, bend and solder copper pipe so if you ever need some plumbing... it's probably best to call someone else.

The installation was modular and easy to break down and move around.

On the software side of things, Node.js handled the Twitter stream and communicated with the Arduino. The Node.js app also maintained a websocket to a browser where some client side Javascript captured the webcam image. 

<img src="/img/portfolio/tmw-wired/Copper-Pipe-Soldering.jpg#center" title="Copper pipe soldering"/>

<img src="/img/portfolio/tmw-wired/Copper-Pipe-Bending.jpg#center" title="Copper pipe soldering"/>

<img src="/img/portfolio/tmw-wired/TMW-Wired-Loop-Electronics-Exposed.jpg#center" title="Arduino in enclosure"/>

<img src="/img/portfolio/tmw-wired/TMW-Wired-Loop-Electronics-Enclosure.jpg#center" title="Arduino in enclosure"/>

##It Lived Again!
In 2014 TMW hosted an Expo showcasing some of our innovation and tech work. I reassembled the game without the RFID capabilities, asking the user to instead enter a Twitter username to play.

<img src="/img/portfolio/tmw-wired/lydia_pang_.jpg#center" title=""/>

<img src="/img/portfolio/tmw-wired/tmwagency.jpg#center" title=""/>

A tribute to the 'victims' remains here: https://twitter.com/tmw_wired

![](/img/portfolio/tmw-wired/expo.gif#center)