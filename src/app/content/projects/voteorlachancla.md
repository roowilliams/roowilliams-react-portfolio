+++
showonlyimage = false
draft = false
image = "img/portfolio/voteorlachancla/featured-image.gif"
date = "2016-11-05T19:59:22+05:30"
title = "Vote or La Chancla"
weight = 2
blurb = "A week-long sprint to save us all."
tech = [
	'HTML5/CSS3',
	'Javascript',
	'Node.js',
	'Twitter API',
	'Gifshot',
	'Live streaming'
]

[[collaborators]]
	name = 'Ilia Isales'
	url = 'http://cargocollective.com/iliaisales'

[[collaborators]]
	name = 'Adrian Ortega'
	url = 'http://adrian-ortega-wqyx.squarespace.com/'

[[collaborators]]
	name = 'Eddie Barrous'
	url = 'http://www.edwardbarrous.com/'

[[links]]
	name = 'CNET'
	url = 'https://www.cnet.com/es/noticias/voteorlachancla-campana-salir-a-votar/'

[[links]]
	name = 'Impresora3DPrinter'
	url = 'http://impresora3dprinter.com/el-chanclazo-3d-contra-trump/2016/11/08/'

[[links]]
	name = 'Barrio'
	url = 'https://esbarrio.com/poplitics/chanclatrump-donald-trump-a-chanclazos/'

+++

A week before the 2016 US presidential election a few of the Latino folk at Anomaly and I got together to see if we could create something that would encourage Latino voters to get out the vote using creative technology. 

We decided to use 'La Chancla' as a device to motivate action.

### The Idea
‘La Chancla’ is an icon of Latino culture and a device that would strike fear into the eyes of every misbehaving child. In my British culture the equivalent force was known as ‘The Slipper’, and I felt its wrath too many times as a kid.

![](/img/portfolio/voteorlachancla/lachancla.gif#center)

<!-- <iframe width="560" height="315" src="https://www.youtube.com/embed/SmD624s-Wxg?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe> -->

Jumping off the hypothesis of ‘could encouraging people to state publicly that they intent to go out and vote make them accountable to take action?’, we designed a system that provided the satisfaction of giving Trump a ‘chanclazo’ around the face with a chancla if you publicly tweeted your intention to go out and vote. Unfortunately we weren’t able to get access to Trump himself for this so we used a 6in tall bobblehead proxy, and created a tiny, 3D printed Chancla fastened to an Arduino-powered machine to do the slapping. Then connected it to Twitter using NodeJS.

![](/img/portfolio/voteorlachancla/wip-03.jpg#center)

<div class="gallery-grid">
	<div class="row">
<div class="col-xs-12 col-sm-6">
	<img src="/img/portfolio/voteorlachancla/3d-printed-device.jpg#center" alt="" />
</div>
<div class="col-xs-12 col-sm-6">
		<img src="/img/portfolio/voteorlachancla/3d-printed-chancla.jpg#center" alt="" />
</div>
</div>
</div>

### Send a Tweet, Slap Trump, Get a GIF

As novel as this was, we came up with a few features to help people engage, and spread this idea. I designed visibility into the participation in the hope that it would encourage others to do the same. I hacked together a way to quickly to create a personalized and sharable asset that we could automatically tweet back to the user as a reward for their participation. We used the language of The Internet; a personalized GIF generated on every interaction that showed the user’s profile image, their tweet and our tiny Trump getting chanclazo’d on their behalf.

A second camera was used to live stream the scene to the site.

<div class="gallery-grid">
<div class="row">
<div class="col-xs-12 col-sm-6">
<img src="/img/portfolio/voteorlachancla/gifs/1.gif#center" alt="" />
</div>
<div class="col-xs-12 col-sm-6">
<img src="/img/portfolio/voteorlachancla/gifs/2.gif#center" alt="" />
</div>
</div>
<div class="row">
<div class="col-xs-12 col-sm-6">
<img src="/img/portfolio/voteorlachancla/gifs/3.gif#center" alt="" />
</div>
<div class="col-xs-12 col-sm-6">
<img src="/img/portfolio/voteorlachancla/gifs/4.gif#center" alt="" />
</div>
</div>
</div>

Mapping the system really helped manage the complexity during the rapid build.

<a href="/img/portfolio/voteorlachancla/system-flow.png#center" data-toggle="lightbox">
	![](/img/portfolio/voteorlachancla/system-flow.png#center)
</a>


<div class="gallery-grid">
	<div class="row">
<div class="col-xs-12 col-sm-6">
	<img src="/img/portfolio/voteorlachancla/wip-01.jpg" alt="" />
</div>
<div class="col-xs-12 col-sm-6">
		<img src="/img/portfolio/voteorlachancla/wip-02.jpg" alt="" />
</div>
</div>
</div>

And then after the election was over, the slapper became useful for other things.

<blockquote class="instagram-media" data-instgrm-captioned data-instgrm-version="7" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://www.instagram.com/p/BPbEoHZB80V/" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_blank">Ouch! It&#39;s still January. The too-soon bot. #arduino #kitkat #maker #january #fitness #dryjanuary #weightloss #weightlossjourney @kitkat_us</a></p> <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">A post shared by Roo Williams (@roowilliams) on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2017-01-18T22:47:05+00:00">Jan 18, 2017 at 2:47pm PST</time></p></div></blockquote> <script async defer src="//platform.instagram.com/en_US/embeds.js"></script>
