  var webpack = require('webpack');
  var path = require('path');
  var CopyWebpackPlugin = require('copy-webpack-plugin');

  var BUILD_DIR = path.resolve(__dirname, 'dist');
  var APP_DIR = path.resolve(__dirname, 'src/app');

  var config = {
    devtool: 'eval-cheap-module-source-map',
    entry: APP_DIR + '/index.jsx',
    output: {
      path: BUILD_DIR,
      filename: 'bundle.js'
    },
    module : {
      rules : [{
          test : /\.jsx?/,
          include : APP_DIR,
          loader : 'babel-loader'
        },
        {
          test: /\.css$/,
          use: [ 'style-loader', 'css-loader', 'autoprefixer-loader' ]
        },
        {
          test: /\.scss$/,
          use: [ 'style-loader', 'css-loader', 'autoprefixer-loader', 'sass-loader' ]
        },
        {
          test: /\.md$/,
          loader: 'file-loader'
        }]
      },
      plugins: [
        new CopyWebpackPlugin([
            {from:'src/img',to:'img'},
            {from:'src/index.html',to:''} 
        ])
      ]
  };

module.exports = config;